# EEE3099_Design_Project


## How to contribute

cd existing_repo
git remote add origin https://gitlab.com/Thomas_GW/eee3099_design_project.git
git push origin master

then go to create new merge request to merge master with main
see [Create a new merge request]
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


## Name
EEE3099F Project

## Description
A dump for all the files that we will contribute to


## Authors and acknowledgment
Thomas Greenwood
Mike Hills
Jana van Rooyan

## License
For open source projects, say how it is licensed.

## Project status
Under development